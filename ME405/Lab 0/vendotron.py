"""@file vendotron.py 
Documentation for / use of myFile.py

@author Eliot Briefer
@date Apr. 22, 2020
"""

from time import sleep
import time
from math import floor
import keyboard

##@brief A dictionary of descriptive messages for each state, used for debugging.1
stateMsgs = {0 : 's0 startup',
             1 : 's1 idle',
             2 : 's2 coin inserted',
             3 : 's3 eject coins',
             4 : 's4 vend cuke',
             5 : 's5 vend popsi',
             6 : 's6 vend spryte',
             7 : 's7 vend dr. pupper',
             8 : 's8 idle message',
             9 : 's9  second beverage',
             10 : 's10 insufficient funds'}

moneyKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
##@brief The last key pressed on the keyboard
last_key = ''
    
def getChange(self, price, payment):
    '''@brief A function to calculate the change from a transaction
    @param price An integer representing the price in cents
    @param payment A dictionary of the payment bills {'pennies': 0, 'ones' : 0, ...}
    @return A dictionary of the same form as payment with the change from the transaction
            returns None if payment is insufficient.'''

##@brief A dictionary of the cent values of each currency denomination
    denomValues = {'hundereds' : 10000,
              'fifties' : 5000,
              'twenties' : 2000,
              'tens' : 1000,
              'fives' : 500,
              'ones' : 100,
              'quarters' : 25,
              'dimes' : 10,
              'nickles' : 5,
              'pennies' : 1}
    
    ##@brief the payment value in cents
    paymentCents = 0 
    for d in payment:
        paymentCents += payment[d] * denomValues[d]
    
    if paymentCents < price:
        return None
    else:
        ##@brief A dictionary of change from the transaction
        chng = {} 
        ##@brief The cents remaining to be turned into change
        centsLeft = paymentCents - price
        for d in denomValues:
            ##@brief The cents value of the current denomination
            v = denomValues[d]
            ##@brief The number of the current denomination needed for change
            n = floor(centsLeft/v)
            chng[d] = n
            centsLeft -= n*v
        
        return chng
        
    
        
def VendotronTask():
    '''
    @brief A generator function that implements a finite state machine of the Vendotron vending machine.
    @details The state machine for the Vendotron is shown below:
    @image html vendotronFSM.png
    '''
    global last_key
    ##@brief The current state of the Vendotron FSM.
    state = 0
    ##@brief The current change inserted into the Vendotron in cents.
    change = 0
    lastTime = time.time()
    currentTime = time.time()
    vended = False
    
    
    while True:
        
        if state == 0:
            change = 0
            lastTime = time.time()
            currentTime = time.time()
            vended = False
            print('Welcome to the Vendotron!')
            print('Press keys 0-9 to enter a penny up to a $100 bill into the Vendotron.')
            print('Press e to get your change back.')
            print('Press p to vend Popsi, c to vend Cuke, s to vend Spryte, and d to vend Dr. Pupper.')
            print('Popsi costs $1.20, Cuke costs $1.00, Spryte costs $0.85, and Dr. Pupper costs $1.10.')
            #print(stateMsgs[state])
            state = 8     # on the next iteration, the FSM will run state 1
            #print(stateMsgs[state])
            
        elif state == 1:
            currentTime = time.time()
            if currentTime - lastTime > 30:
                state = 8
                #print(stateMsgs[state])
            elif last_key == 'c':
                state = 4
                #print(stateMsgs[state])
            elif last_key == 'p':
                state = 5
                #print(stateMsgs[state])
            elif last_key == 's':
                state = 6
                #print(stateMsgs[state])
            elif last_key == 'd':
                state = 7
                #print(stateMsgs[state])
            elif vended and change > 0:
                state = 9
                #print(stateMsgs[state])
            elif last_key in moneyKeys:
                state = 2
                #print(stateMsgs[state])
            elif last_key == 'e':
                state = 3
                #print(stateMsgs[state])
            
        elif state == 2:
            #coin inserted
            keyValues = {'0' : 1,
                         '1' : 5,
                         '2' : 10,
                         '3' : 25,
                         '4' : 100,
                         '5' : 500,
                         '6' : 1000,
                         '7' : 2000,
                         '8' : 5000,
                         '9' : 10000}
            
            change += keyValues[last_key]
            last_key = ''
            state = 1
            print('You have: $' + str(change/100))
            lastTime = time.time()
            
        elif state == 3:
            #eject coins
            print("Here's your: $" + str(change/100))
            state = 8
            
        elif state == 4:
            if change >= 100:
                change -= 100
                vended = True
                last_key = ''
                print("Here's your Cuke!")
                state = 1
            else:
                state = 10
                #print(stateMsgs[state])
        elif state == 5:
            if change >= 120:
                change -= 120
                vended = True
                last_key = ''
                print("Here's your Popsi!")
                state = 1
            else:
                state = 10
                #print(stateMsgs[state])
            
        elif state == 6:
            if change >= 85:
                change -= 85
                vended = True
                last_key = ''
                print("Here's your Spryte!")
                state = 1
            else:
                state = 10
                #print(stateMsgs[state])
            
        elif state == 7:
            if change >= 110:
                change -= 110
                vended = True
                last_key = ''
                print("Here's your Dr. Pupper!")
                state = 1
            else:
                state = 10
                #print(stateMsgs[state])
            
        elif state == 8:
            print('Try Cuke Today!')
            lastTime = time.time()
            vended = False
            state = 1
            last_key = ''
            
        elif state == 9:
            print('You have: $' + str(change/100))
            print('Select a second beverage!')
            state = 1
            vended = False
            lastTime = time.time()
        elif state == 10:
            print('Insufficient funds.')
            last_key = ''
            lastTime = time.time()
            state = 1
        else:
            pass
        
        yield(state)
    
def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name
    
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)
keyboard.on_release_key("8", callback=kb_cb)
keyboard.on_release_key("9", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("e", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)



if __name__ == "__main__":
    
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')