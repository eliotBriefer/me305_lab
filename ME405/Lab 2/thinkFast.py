"""@file thinkFast.py
@brief thinkFast.py measures the users reaction time using the built in LED and the user button on the Nucleo.
@details 
@author Eliot Briefer
@date May 2, 2020
"""

import utime, pyb, random

class reactionTimerA:
    '''
    @brief A class that measures the reaction time of the user as described in part A of the lab manual.
    @details The reactionTimerA class uses an interupt triggered byt a falling edge on the input pin
    and utime.ticks_diff() to measure the time between when the LED on the Nucleo is turned on and the time
    when the user presses the button.
    '''
    def __init__(self, inputPin, outputPin):
        '''
        @brief A constructor that initilalizes a reactionTimerA object.
        @param inputPin A pyb.Pin object that the reactionTimerA will use to measure the users reaction time.
        @param outputPin A pyb.Pin object that the reactionTimerA will use to signal that the user should trigger the inputPin
        '''
        
        self.targetTime = utime.ticks_us() #the time when self.outputPin was set to high. Used to calculate reaction time.
        self.measuredTime = utime.ticks_us() #temporary value only used for more accurate measurment
        self.lastReactionTime = -1 #-1 indicates that no interupt has occured on current led cycle
        self.waitTime = random.randrange(2*10**6, 3*10**6) #the time (in micro seconds) between flashes.
        self.numMeasurements = 0 #The number of good reaction time measurments.
        self.averageReactionTime = 0 #The average reaction time of the user
        ##@brief The state of the reactionTimerA state machine.
        self.state = 0
        
        ##@brief The pyb.Pin object to accept user input from
        self.inputPin = inputPin
        ##@brief The pyb.Pin object to signal to the user that they should trigger the input pin
        #@details self.outputPin should be control an LED, speaker, or some other way of physically communicating with the user.
        self.outputPin = outputPin
        self.outputPin.low() #turn off the output pin
        ##@brief The interupt fucntion triggered by a falling edge on self.inputPin
        self.inputFallingInt = pyb.ExtInt(self.inputPin, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.onInputFallingFCN)
        
    def runFSM(self):
        if self.state == 0:
            #waiting random amount of time until turning on the led.
            now = utime.ticks_us()
            diff = utime.ticks_diff(now, self.targetTime)
            if diff >= self.waitTime + 1*10**6: #+1 sec to acount for the time the led is on
                self.state = 1
                self.targetTime = utime.ticks_us()
                self.outputPin.high() #turn on the output pin
        if self.state == 1:
            #led is on, wait 1 second
            now = utime.ticks_us()
            diff = utime.ticks_diff(now, self.targetTime)
            if diff >= 1*10**6: #led has been on for 1 second
                self.state = 2
                self.outputPin.low() #turn off the output pin
                
                
        if self.state == 2:
            #led is off, calculate reaction time and reset.
            if self.lastReactionTime != -1: #if the user reacted
                #Recalculate the average reaction time
                self.averageReactionTime = (self.averageReactionTime * self.numMeasurements + self.lastReactionTime) / (self.numMeasurements + 1)
                self.numMeasurements += 1
            #Reset for next cycle
            self.waitTime = random.randrange(2*10**6, 3*10**6)
            self.lastReactionTime = -1 #-1 indicates that no interupt has occured on current led cycle
            self.state = 0
        
    def onInputFallingFCN(self, IRQ_src):
        '''
        @brief A function that records the time in micro seconds when the user first triggers the input pin.
        @param IRQ_src The pin number that triggered the inturrupt.
        '''
        self.measuredTime = utime.ticks_us() #first save the current time, so as not to waist time on an if statment, which would lead to inacurate measurement
        if self.lastReactionTime == -1 and self.state == 1: #only capture first interupt every time the led turns on
            self.lastReactionTime = utime.ticks_diff(self.measuredTime, self.targetTime)
        
class reactionTimerB:
    '''
    @brief A class that measures the reaction time of the user as described in part A of the lab manual.
    @details The reactionTimerA class uses an interupt triggered byt a falling edge on the input pin
    and utime.ticks_diff() to measure the time between when the LED on the Nucleo is turned on and the time
    when the user presses the button.
    '''
    def __init__(self, inputPin, outputPin, tmr):
        '''
        @brief A constructor that initilalizes a reactionTimerA object.
        @param inputPin A pyb.Pin object that the reactionTimerA will use to measure the users reaction time.
        @param outputPin A pyb.Pin object that the reactionTimerA will use to signal that the user should trigger the inputPin
        @param tmr A pyb.Timer object used for measuring reaction time.
        '''
        
        self.targetTime = utime.ticks_us() #the time when self.outputPin was set to high. Used to calculate reaction time.
        self.measuredTime = utime.ticks_us() #temporary value only used for more accurate measurment
        self.lastReactionTime = -1 #-1 indicates that no interupt has occured on current led cycle
        self.waitTime = random.randrange(2*10**6, 3*10**6) #the time (in micro seconds) between flashes.
        self.numMeasurements = 0 #The number of good reaction time measurments.
        self.averageReactionTime = 0 #The average reaction time of the user
        ##@brief The state of the reactionTimerA state machine.
        self.state = 0
        
        ##@brief The pyb.Pin object to accept user input from
        self.inputPin = inputPin
        ##@brief The pyb.Pin object to signal to the user that they should trigger the input pin
        #@details self.outputPin should be control an LED, speaker, or some other way of physically communicating with the user.
        self.outputPin = outputPin
        self.outputPin.low() #turn off the output pin
        ##@brief pyb.Timer used to measure the reaction time
        self.tmr = tmr
        self.tmrCh1 = self.tmr.channel(1, mode = pyb.Timer.IC, callback = self.ch1CallBack)
        ##@brief The interupt fucntion triggered by a falling edge on self.inputPin
        self.inputFallingInt = pyb.ExtInt(self.inputPin, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.onInputFallingFCN)
        
    def runFSM(self):
        if self.state == 0:
            #waiting random amount of time until turning on the led.
            now = utime.ticks_us()
            diff = utime.ticks_diff(now, self.targetTime)
            if diff >= self.waitTime + 1*10**6: #+1 sec to acount for the time the led is on
                self.state = 1
                self.targetTime = utime.ticks_us()
                self.outputPin.high() #turn on the output pin
        if self.state == 1:
            #led is on, wait 1 second
            now = utime.ticks_us()
            diff = utime.ticks_diff(now, self.targetTime)
            if diff >= 1*10**6: #led has been on for 1 second
                self.state = 2
                self.outputPin.low() #turn off the output pin
                
                
        if self.state == 2:
            #led is off, calculate reaction time and reset.
            if self.lastReactionTime != -1: #if the user reacted
                #Recalculate the average reaction time
                self.averageReactionTime = (self.averageReactionTime * self.numMeasurements + self.lastReactionTime) / (self.numMeasurements + 1)
                self.numMeasurements += 1
            #Reset for next cycle
            self.waitTime = random.randrange(2*10**6, 3*10**6)
            self.lastReactionTime = -1 #-1 indicates that no interupt has occured on current led cycle
            self.state = 0
        
    def ch1CallBack(self, )
    def onInputFallingFCN(self, IRQ_src):
        '''
        @brief A function that records the time in micro seconds when the user first triggers the input pin.
        @param IRQ_src The pin number that triggered the inturrupt.
        '''
        self.measuredTime = utime.ticks_us() #first save the current time, so as not to waist time on an if statment, which would lead to inacurate measurement
        if self.lastReactionTime == -1 and self.state == 1: #only capture first interupt every time the led turns on
            self.lastReactionTime = utime.ticks_diff(self.measuredTime, self.targetTime)
            
            
if __name__ == '__main__':
     led = pyb.Pin (pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP) #nucleo led output pin
     usrbtn = pyb.Pin(pyb.Pin.cpu.C13) #nucleo user input button pin
     tim2 = pyb.Timer(2, prescaler = 83, period = 10000000) #configure a microsecond timer that counts for five seconds
     reactionTimerB = reactionTimerB(usrbtn, led, tim2)
     try:
         while True:
             reactionTimerA.runFSM()
     finally: #after user presses ctrl+c
        if reactionTimerA.numMeasurements == 0:
            print('Error: No reaction time measurments recorded!')
        else:
            print('Average reaction time: ' + str(reactionTimerA.averageReactionTime * 10**-6) + ' seconds after ' + str(reactionTimerA.numMeasurements) + ' trys.')
            print()
        