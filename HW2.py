'''
@file       HW2.py
@brief A    state machine for an elevator in a two story building
@details    The elevator system is modeled as a motor for moving up or down,
            a button to request each floor, and sensors to detect when the
            elevator is at each floor. The button presses and sensor activations
            are random to simulate user input and a physical system. 
            Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/HW2.py
@author     Eliot Briefer
@date       1/26/2021

'''

import time, random

def button_1():
    '''
    @brief      function to check the state of button_1.
    @return     a boolean representing the state of the button (True = pressed, False = not pressed).
    '''
    return random.choice([True, False])

def button_2():
    '''
    @brief      function to check the state of button_2.
    @return     a boolean representing the state of the button (True = pressed, False = not pressed).
    '''
    return random.choice([True, False])

def floor_1():
    '''
    @brief      function to check the state of floor_1 sensor.
    @return     a boolean representing the state of the button (True = at floor 1 , False = not at floor 1).
    '''
    return random.choice([True, False])

def floor_2():
    '''
    @brief      function to check the state of floor_2 sensor.
    @return     a boolean representing the state of the button (True = at floor 1 , False = not at floor 1).
    '''
    return random.choice([True, False])

def motor_cmd(cmd):
    '''
    @brief      function to control the elevator motor.
    @param cmd  the command to give to motor ('UP', 'DOWN', or 'STOP')
    '''
    
    if cmd == 'UP':
        print('Moving Up')
    elif cmd == 'DOWN':
        print('Moving Down')
    elif cmd == 'STOP':
        print('Motor stop')
    else:
        print('Invalid motor command!')
        
if __name__ == "__main__":
    ## @brief   The current state of the elevator finite state machine
    state = 0
    
    while True:
        try:
            if state == 0:
                print('s0')
                motor_cmd('DOWN')
                if floor_1:
                    motor_cmd('STOP')
                    state = 1
                
            elif state == 1:
                print('s1')
                if button_2():
                    motor_cmd('UP')
                    state = 2
                
            elif state == 2:
                print('s2')
                if floor_2:
                    motor_cmd('STOP')
                    state = 3
                    
            elif state == 3:
                print('s3')
                if button_1():
                    motor_cmd('DOWN')
                    state = 0
            else:
                print('Invalid State')
                state = 0
                
            time.sleep(0.5)
            
        except KeyboardInterrupt:
            break
    print('Exit')