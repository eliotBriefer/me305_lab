'''
@file pid.py
@brief A class to implement a standard PID controller.
@details The pid.ClosedLoop class is used by the controller task, controller.Controller
to calculate the needed duty cycle of the motor, based on an input position and velocity
error signal.
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/pid.py
@author Eliot Briefer
@ingroup week3
'''
class ClosedLoop:
    '''
    @brief A Class that implements a standard PID controller.
    @details The ClosedLoop class is used to calculate the response to an
    error signal using PID control. The ClosedLoop object also evlauates its 
    performance over time using the Remon sum of the squares of the velocity and 
    postion error over time.
    '''
    def __init__(self, kp = 0, ki = 0, kd = 0, sat = 100):
        '''
        @brief A method to initialize a ClosedLoop object.
        @param kp The proportional gain, Kp, of the PID controller.
        @param ki The integral gain, Ki, of the PID controller.
        @param kd The derivative gain, Kd of the PID controller.
        @param sat The maximum absolute vlaue of the response.
        '''
        ##@brief The proportional gain, Kp, of the PID controller.
        self.kp = kp
        #@brief The integral gain, Ki, of the PID controller.
        self.ki = ki
        ##@brief The derivative gain, Kd of the PID controller.
        self.kd = kd
        ##@brief The integral of the position error over time (Reiman sum)
        self.iError = 0
        ##@brief J (performance metric) times K (number of updates)
        self.jk = 0
        ##@brief Number of updates since self.reset_J has been called.
        self.k = 0
        ##@brief The maximum absolute vlaue of the response.
        self.sat = abs(sat)
        
    def update(self, posError, velError):
        '''
        @brief A method to update the response of the PID controller to a given error signal.
        @param posError The closed loop position error: desired - measured.
        @param velError The closed loop velocity error: desired - measured.
        @return The PID controller response to the given error signal.
        '''
        self.k += 1
        self.iError += posError
        self.jk += posError**2 + velError**2
        response = self.kp*posError + self.kd*velError + self.iError*self.ki
        if abs(response) > self.sat: #check for saturation
            response = self.sat * response / abs(response)
        return response
    
    def get_Kp(self):
        '''
        @brief Get the proortional gain value, Kp.
        @return The proportional gain.
        '''
        return self.kp
    
    def get_Ki(self):
        '''
        @brief Get the integral gain value, Ki.
        @return The integral gain.
        '''
        return self.ki
    
    def get_Kd(self):
        '''
        @brief Get the derivative gain value, Kd.
        @return The derivative gain.
        '''
        return self.kd
    
    def set_Kp(self, kp):
        '''
        @brief Set the proortional gain value, Kp.
        '''
        self.kp = kp
        
    def set_Ki(self, ki):
        '''
        @brief Set the integral gain value, Ki.
        '''
        self.ki = ki
        
    def set_Kd(self, kd):
        '''
        @brief Set the derivative gain value, Kd.
        '''
        self.kd = kd
        
    def get_J(self):
        '''
        @brief Get the PID controller performance metric, J.
        @return The PID controller performance metric, J.
        '''
        return self.jk / self.k
    
    def reset_J(self):
        '''
        @brief Reset the performance metric, J.
        '''
        self.k = 0
        self.jk = 0
        
    
        
        