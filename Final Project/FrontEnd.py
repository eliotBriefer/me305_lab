'''
@file FFX0.py
@brief The PC side user interface to communicate with the nucleo over serial and display data.
@details The FrontEnd.UI class is a state machine that sends and recieves ASCII characters over
serial coms with the nucleoSerial.nucleoSerial task running on the Nucleo. Once the Nucleo is
finished transmitting motor position and velocity data, FrontEnd.UI displays the data in a graph 
and saves it to a .csv file.
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/FrontEnd.py
@Author Eliot Briefer
'''

import serial
import time
from csvResampler import writeCSV, readCSV
import matplotlib.pyplot as plt


class UI:
    '''
    @brief The UI class is the PC side user interface for communicating with the nucleo over serial.
    @details The UI class takes keyboard inputs and passes them to the Nucleo over serial.
    It also listens for motor position data from the Nucleo and displays the position data in
    a Matlab style plot.
    '''
    
    ##@brief A dictionary of messages to display at each state change.
    stateMessages = {
        0 : "DESKTOP: Waiting for Input",
        1 : "DESKTOP: Reading Output"
        }
    
    def __init__(self, com, startState = 0):
        '''
        @brief A method to initialize a UI object.
        @param com the serial com number used to communicate with the Nucleo, should be 'COM#'.
        @param startState The state to start the UI state machine in.
        '''
        ##@brief The serial com number used to communicate with the Nucleo.
        self.port = com
        ##@brief The Serial object used to read an write to serial.
        self.ser = serial.Serial(port=self.port, baudrate=115273,timeout=1)
        ##@brief The current state of the UI state machine.
        self.state = startState
        ##@brief The last time a state transition occured, in floating point seconds.
        self.lastTransition = time.time()
        ##@brief A nested list of the motor position data transimited over serial.
        self.data = []
        
    def runStateMachine(self):
        '''
        @brief A method to run through the UI state machine logic once.
        '''
        t = time.time() - self.lastTransition
        if self.state == 0:
            self.sendChar()
            self.transitionStates(1)
        elif self.state == 1:
            line = self.readLine()
            print(self.readLine())
            if line != '':
                self.data.append([float(word.strip()) for word in line.split(',')])
            if t >= 20: 
                self.transitionStates(0)
                toFile = open('log.csv', 'w')
                writeCSV(toFile, self.data)
                toFile.close()
                refFile = open('rescaledReference.csv')
                refData = readCSV(refFile)
                refVelocity = []
                refPosition = []
                refTimes = []
                for line in refData:
                    refVelocity.append(line[1])
                    refPosition.append(line[2])
                    refTimes.append(line[0])
                
                #refPosition = [0, 9000]
                #refTimes = [0, 10]
                #refVelocity = [300, 300]
                
                velocity = []
                position = []
                times = []
                for line in self.data:
                    velocity.append(line[1]*60)
                    position.append(line[2]*360)
                    times.append(line[0])
                #print(position)
                #print(times)
                plt.figure(1)
                plt.plot(times, position, 'r-', refTimes, refPosition, 'k--')
                plt.ylabel('Position [deg]')
                plt.xlabel('Time [s]')
                plt.title('PID Position Tracking')
                plt.axis([0, 15, 0, 8000])
                plt.show()
                
                plt.figure(2)
                plt.plot(times, velocity, 'r-', refTimes, refVelocity, 'k--')
                plt.ylabel('Velocity [RPM]')
                plt.xlabel('Time [s]')
                plt.title('Proportional Velocity Control')
                plt.axis([0, 15, -1000, 1000])
                plt.show()
                self.data = []
                
            #print(self.data)
            
            
    def sendChar(self):
        '''
        @brief A method to prompt the user to input a character into the console and then send it over serial as an ASCII character.
        '''
        inv = input('Give me a character: ')
        self.ser.write(str(inv).encode('ascii'))
    
    def readLine(self):
        '''
        @brief A method to read a line from serial.
        @return The line read from serial.
        '''
        return self.ser.readline().decode('ascii')
    
    def transitionStates(self, st, customMessage = ""):
        '''
        @brief A method to transition between states in the UI state machine.
        @param st The state number to transition to.
        @customMessage An additional message to print before the state transition message.
        '''
        self.state = st
        if customMessage != "": print(customMessage)
        print(self.stateMessages[st])
        self.lastTransition = time.time()

if __name__ == '__main__':
    UI = UI(com = 'COM4')
    while True:
        UI.runStateMachine()
    UI.ser.close()