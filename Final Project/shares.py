'''
@file shares.py
@brief A script to store data for sharing between nucleo tasks.
@details This script is used to share data between the serial coms task, nucleoSerial.nucleoSerial,
and the controller task, controller.Controller to prevent ethier of the tasks from directly acsessing
eachothers' data.
Link to source code: https://bitbucket.org/ebriefer/me305_lab/src/master/Final%20Project/shares.py
@author Eliot Briefer
@ingroup week4
'''
#shares.py for all task level communication
##@brief A list of the most recent motor state. Format [time stamp (seconds), velocity (rotations per second), position (rotation)]
motorState = []
##@brief A boolean for wether or not the motorstate has been updated. True = motorState has been updated since last check.
newMotorState = False

##@brief A boolean for wether or not to collect data. True = controller and serial tasks should transition to data collection states.
collectData = False

def getMotorState():
    '''
    @brief A method used by nucleoSerial to get the motor position and velocity
    @return The most recent motor state. Format [time stamp (seconds), velocity (rotations per second), position (rotation)]
    '''
    global newMotorState
    if motorState == [] or not newMotorState:
        return None
    else:
        newMotorState = False
        return motorState
    
def logMotorState(state):
    '''
    @brief A method used by Controller to update the recorded motor state.
    @param state The state of the motor. Format [time stamp (seconds), velocity (rotations per second), position (rotation)]
    '''
    global motorState
    global newMotorState
    motorState = state
    newMotorState = True
