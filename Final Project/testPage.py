## \mainpage ME305 INTRODUCTION TO MECHATRONICS
#
#By Eliot Briefer
#
#This website provides documentation for code written for Calpoly’s ME305 Introduction to
#Mechatronics course. All code is written in python, using the micropython library to run on
#a Nucleo micro controller.
